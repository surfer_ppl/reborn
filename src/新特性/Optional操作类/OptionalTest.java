package 新特性.Optional操作类;

import java.util.Optional;

import 设计模式.建造者模式.People;
import 设计模式.建造者模式.People2;
import 设计模式.建造者模式.People3;

public class OptionalTest {
    @SuppressWarnings("unused")
    public static void main(String[] args) throws Exception {
        /**
         * java中为了避免出现NPE(空指针异常)，经常需要对对象或者字符串进行非空判断
         */
        People people = null;
        // 这一步肯定会报错
        // people.getAddress();
        /**
         * 传统判空
         */
        if (people != null) {
            System.out.println(people.getAddress());
        } else {
            System.out.println("peopl为null");
        }

        /**
         * Optionalj简介
         * 1、构造函数
         *  private Optional(T value) {
         *      this.value = Objects.requireNonNull(value);
         *  }
         *  public static <T> T requireNonNull(T obj) {
         *     if (obj == null)
         *          throw new NullPointerException();
         *      return obj;
         *  }
         *  tip：Optional(T value)，即构造函数，它是private权限的，不能由外部调用的,在构造的时候，就直接判断其值是否为空
         * 
         * 
         * 2、of(T value)方法：
         * 
         *  public static <T> Optional<T> of(T value) {
         *     return new Optional<>(value);
         *  }           
         *  tip：函数内部调用了构造函数。根据构造函数的源码我们可以得出两个结论:
         *       ① 通过of(T value)函数所构造出的Optional对象，当Value值为空时，依然会报NullPointerException
         *       ② 通过of(T value)函数所构造出的Optional对象，当Value值不为空时，能正常构造Optional对象。
         *  
         *  3、empty()方法,返回EMPTY对象
         *  private static final Optional<?> EMPTY = new Optional<>();
         *  public static<T> Optional<T> empty() {
         *      @SuppressWarnings("unchecked")
         *      Optional<T> t = (Optional<T>) EMPTY;
         *      return t;
         *  }
         *  
         *  4、ofNullable(T value)方法：
         *  public static <T> Optional<T> ofNullable(T value) {
         *     return value == null ? empty() : of(value);
         *  } 
         *  tip:相比较of(T value)的区别就是，当value值为null时，of(T value)会报NullPointerException异常；
         *      ofNullable(T value)不会throw Exception，ofNullable(T value)直接返回一个EMPTY对象。
         *      当我们在运行过程中，不想隐藏NullPointerException。而是要立即报告，这种情况下就用Of函数。
         *      但是这样的场景真的很少。
         *  5、orElse(T other)、orElseGet(Supplier<? extends T> other)、orElseThrow(Supplier<? extends X> exceptionSupplier)
         *  这三个函数放一组进行记忆，都是在构造函数传入的value值为null时，进行调用的。
         *  @Test
         *  public void test() {
         *     User user = null;
         *     user = Optional.ofNullable(user).orElse(createUser());
         *     user = Optional.ofNullable(user).orElseGet(() -> createUser());
         *  
         *  }
         *  public User createUser(){
         *      User user = new User();
         *      user.setName("zhangsan");
         *      return user;
         *  } 
         * 
         *  这三个个函数的区别：
         *  当user值不为null时，orElse函数依然会执行createUser()方法，
         *  而orElseGet函数并不会执行createUser()方法，大家可自行测试。
         *  至于orElseThrow，就是value值为null时,直接抛一个异常出去，用法如下所示
         *  
         *  6、isPresent()和ifPresent(Consumer<? super T> consumer)
         *  
         *  isPresent即判断value值是否为空，而ifPresent就是在value值不为空时，做一些操作。
         *  
         *  public boolean isPresent() {
         *      return value != null;
         *  }
         *  
         *  public void ifPresent(Consumer<? super T> consumer) {
         *      if (value != null)
         *        consumer.accept(value);
         *  }
         *  
         *  7、filter(Predicate<? super T> predicate)
         *  filter 方法接受一个 Predicate 来对 Optional 中包含的值进行过滤，
         *  如果包含的值满足条件，那么还是返回这个 Optional；否则返回 Optional.empty
         *  
         *  Optional<User> user1 = Optional.ofNullable(user).filter(u -> u.getName().length()<6);
         *  如果user的name的长度是小于6的，则返回。如果是大于6的，则返回一个EMPTY对象
         *  
         *  
         *  8、map(Function<? super T, ? extends U> mapper)和flatMap(Function<? super T, Optional> mapper)
         *  如果optional不为空，则将optional中的对象 t 映射成另外一个对象 u，并将 u 存放到一个新的optional容器中。
         *  在optional不为空的情况下，将对象t映射成另外一个optional
         *  区别：map会自动将u放到optional中，而flatMap则需要手动给u创建一个optional
         *  
         *  
         */

        People2 people2 = null;

        People3 people3 = People3.builder().name("丁道安").gender("男").age(21).address("全性").build();

        // 举例一、
        if (people2 != null) {
            System.out.println(people.getAddress());
        } else {
            System.out.println("peopl为null");
        }

        String orElseThrow =
            Optional.ofNullable(people3).map(p -> p.getAddress()).orElseThrow(() -> new Exception("错误123456"));
        System.out.println(orElseThrow);

        Optional<String> addressStrOp = Optional.ofNullable(people3).map(People3::getAddress);
        Optional<People3> people3Op = Optional.ofNullable(people3);
     
        if (addressStrOp.isPresent()) {
            System.out.println(addressStrOp.get());
        }

    }

}
