package 新特性.Lambda表达式;

/**
 * Lambda 允许把函数作为一个方法的参数（函数作为参数传递进方法中）。
 * 使用 Lambda 表达式可以使代码变的更加简洁紧凑。
 * 语法：
 *      (parameters) -> expression 
 *      或 
 *      (parameters) ->{ statements; }
 *  
 *  特征：
 *      可选类型声明：不需要声明参数类型，编译器可以统一识别参数值。
 *      可选的参数圆括号：一个参数无需定义圆括号，但多个参数需要定义圆括号。
 *      可选的大括号：如果主体包含了一个语句，就不需要使用大括号。
 *      可选的返回关键字：如果主体只有一个表达式返回值则编译器会自动返回值，大括号需要指定表达式返回了一个数值。
 * @author Edward
 * @date 2022/08/31
 */
public class Lambda表达式基本使用 {
    //一些简单的操作
    public static void main(String[] args) {
        Lambda表达式基本使用 tester = new Lambda表达式基本使用();
        
        // 类型声明,写return
        MathOperation add = (int a, int b) -> {
            return a + b;
        };
        // 类型不声明，不写return
        MathOperation sub = (a, b) -> a - b;
        // 类型声明，不写return
        MathOperation mult = (int a, int b) -> a * b;
        // 类型不声明，写return
        MathOperation div = (a, b) -> {
            return a / b;
        };
        
        System.out.println("10 + 5 = " + tester.operate(10, 5, add));
        System.out.println("10 - 5 = " + tester.operate(10, 5, sub));
        System.out.println("10 x 5 = " + tester.operate(10, 5, mult));
        System.out.println("10 / 5 = " + tester.operate(10, 5, div));
        
        //不用括号
        GreetingService greetService1 = message ->
        System.out.println("Hello " + message);
          
        // 用括号
        GreetingService greetService2 = (message) ->
        System.out.println("Hello " + message);
          
        greetService1.sayMessage("Runoob");
        greetService2.sayMessage("Google");
        
        
    }
    //定义一个数字操作接口
    interface MathOperation {
        int operation(int a, int b);
    }
    //定义一个
    interface GreetingService {
        void sayMessage(String message);
    }
    
    private int operate(int a, int b, MathOperation mathOperation){
        return mathOperation.operation(a, b);
     }
}
