package 新特性.Lambda表达式;

/**
 * 
 * @author Edward
 * @date 2022/08/31
 */
public class Lambda表达式变量作用域 {

    static String salutation = "Hello! ";

    public static void main(String[] args) {
        // 可以直接在 lambda 表达式中访问外层的局部变量
        String salutation2 = "Hi!";
        GreetingService greetService1 = message -> System.out.println(salutation + salutation2 + message);
        greetService1.sayMessage("Runoob");
        // ambda 表达式的局部变量可以不用声明为 final，但是必须不可被后面的代码修改（即隐性的具有 final 的语义）
        int num = 1;  
        Converter<Integer, String> s = (param) -> System.out.println(String.valueOf(param + num));
        s.convert(2);
        //num = 5; //这里会导致编译错误，一旦被lambda作为局部变量引用就不可再被修改
    }

    interface GreetingService {
        void sayMessage(String message);
    }

    interface Converter<T1, T2> {
        void convert(int i);
    }
}
