package 新特性.方法引用;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * Lambda表达式的主体只有一条语句时,程序不仅可以省略包含主体的花括号，还可以通过英文双冒号::的语法格式来引用方法和构造器 
 * 方法引用通过方法的名字来指向一个方法。 方法引用可以使语言的构造更紧凑简洁，减少冗余代码。
 * 本质就是对Lambda表达式的主体部分已存在的方法进行直接引用
 * 
 * +----------------+ +-------------------------------------------------+ +----------------------+ 
    类名引用普通方法    (arg1,arg2,..)->ClassName.普通方法名(arg1,arg2,..)   ClassName::普通方法名 
   +----------------+ +-------------------------------------------------+ +----------------------+
   +----------------+ +-------------------------------------------------+ +----------------------+ 
    类名引用普通方法    (arg1,arg2,..)->ClassName.静态方法名(arg1,arg2,..)   ClassName::静态方法名 
   +----------------+ +-------------------------------------------------+ +----------------------+ 
   +----------------+ +-------------------------------------------------+ +----------------------+ 
    实例名引用方法      (arg1,arg2,..)->ObjectName.实例方法名(arg1,arg2,..)  ObjectName::实例方法名 
   +----------------+ +-------------------------------------------------+ +----------------------+ 
   +----------------+ +-------------------------------------------------+ +----------------------+ 
    构造器引用         (arg1,arg2,..)->new ClassName(arg1,arg2,..)         ClassName::new 
   +----------------+ +-------------------------------------------------+ +----------------------+
 * 
 * @author Edward
 * @date 2022/08/31
 */
public class 方法引用 {
    public static void main(String[] args) {
        // 构造器引用 它的语法是Class::new，或者更一般的Class< T >::new实例如下：
        final Car car = Car.create(Car::new);
        List<Car> cars = Arrays.asList(car);
        // 静态方法引用：它的语法是Class::static_method
        cars.forEach(Car::collide);
        // 特定类的任意对象的方法引用：它的语法是Class::method实例如下：
        cars.forEach(Car::repair);
        // 特定对象的方法引用：它的语法是instance::method实例如下：
        final Car police = Car.create(Car::new);
        cars.forEach(police::follow);

        // 使用Lambda来表示
        cuaulateMethod(-10, (num) -> Math.abs(num));
        // 使用方法引用
        cuaulateMethod(-20, Math::abs);
    }

    public static void cuaulateMethod(int num, CaculateI caculate) {
        System.out.println(caculate.caculate(num));
    }
}

class Car {
    // Supplier是jdk1.8的接口，这里和lamda一起使用
    public static Car create(final Supplier<Car> supplier) {
        return supplier.get();
    }

    public void follow(final Car another) {
        System.out.println("Following the " + another.toString());
    }

    public static void collide(final Car car) {
        System.out.println("Collided " + car.toString());
    }

    public void repair() {
        System.out.println("Repaired " + this.toString());
    }
}

@FunctionalInterface
interface CaculateI {
    public int caculate(int num);
}
