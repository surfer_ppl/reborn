package 新特性.Stream流;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Stream 使用一种类似用 SQL 语句从数据库查询数据的直观方式来提供一种对 Java 集合的运算和表达。 
 * 这种风格将要处理的元素集合看作一种流， 流在管道中传输， 并且可以在管道的节点上进行处理， 比如筛选， 排序，聚合等。
 * 元素流在管道中经过中间操作（intermediate operation）的处理，最后由最终操作(terminal operation)得到前面处理的结果。
 * 
 * 一、对于中间操作，又可以分为有状态的操作和无状态操作：
 *   1、无状态的操作是指当前元素的操作不受前面元素的影响。
 *   2、有状态的操作是指当前元素的操作需要等所有元素处理完之后才能进行。
 * 二、对于结束操作，又可以分为短路操作和非短路操作，具体如下：
 *   1、短路操作是指不需要处理完全部的元素就可以结束。
 *   2、非短路操作是指必须处理完所有元素才能结束。
 * 三、大致操作如下
 * +--------------------+       +------+   +------+   +---+   +-------+
   | stream of elements +-----> |filter+-> |sorted+-> |map+-> |collect|
   +--------------------+       +------+   +------+   +---+   +-------+   
 *
 * @author Edward
 * @date 2022/09/01
 */
public class StreamTest {
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        /**
         * forEach
         * Stream 提供了新的方法 'forEach' 来迭代流中的每个数据。以下代码片段使用 forEach 输出了10个随机数：
         */
        Random random = new Random();
        
        
        
        
        
        
        
        
        
        
        
        
        /**
         * 创建Stream流使用集合创建 & 使用数组创建
         */
        List<Integer> list = Arrays.asList(5, 2, 3, 1, 4);
        Stream<Integer> stream = list.stream();

        String[] array = {"ab", "abc", "abcd", "abcde", "abcdef"};
        Stream<String> stream2 = Arrays.stream(array);
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /**
         * 百度摘抄的20道练习题
         */
        List<Student> students = StudentList.getStudents();
        // 题目1：筛选学生不及格次数3次及3次以上的学生列表
        List<String> studentS = students.stream().filter(student -> student.getFailCount() >= 3).map(student -> student.getName())
            .collect(Collectors.toList());
        System.out.println(studentS);
        

    }
}
