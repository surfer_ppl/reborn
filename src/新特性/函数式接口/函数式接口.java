package 新特性.函数式接口;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * 函数式接口(Functional Interface)就是一个有且仅有一个抽象方法，但是可以有多个非抽象方法的接口。
 * 
 * 函数式接口可以被隐式转换为 lambda 表达式。
 * 
 * 方法引用（实际上也可认为是Lambda表达式）
 * 
 * 常见的函数式接口： java.lang.Runnable java.util.Comparator java.util.function包下的接口
 * 
 * @author Edward
 * @date 2022/09/01
 */
public class 函数式接口 {

    public static void main(String[] args) {
        String message = "早上好！";
        // 1.8之前使用匿名内部类new接口
        GreetingService greetingService = new GreetingService() {
            @Override
            public void sayMessage(String message) {
                System.out.println("Hello," + message);

            }
        };
        greetingService.sayMessage(message);

        // 使用Lambda表达式
        GreetingService greetingService1 = (msg) -> {
            System.out.println("Hello," + msg);
        };
        greetingService1.sayMessage(message);
        
        
        //Function包下的Predicate接口测试
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        // Predicate<Integer> predicate = n -> true
        // n 是一个参数传递到 Predicate 接口的 test 方法
        // n 如果存在则 test 方法返回 true

        System.out.println("输出所有数据:");

        // 传递参数 n
        eval(list, n -> true);

        // Predicate<Integer> predicate1 = n -> n%2 == 0
        // n 是一个参数传递到 Predicate 接口的 test 方法
        // 如果 n%2 为 0 test 方法返回 true

        System.out.println("输出所有偶数:");
        eval(list, n -> n % 2 == 0);

        // Predicate<Integer> predicate2 = n -> n > 3
        // n 是一个参数传递到 Predicate 接口的 test 方法
        // 如果 n 大于 3 test 方法返回 true

        System.out.println("输出大于 3 的所有数字:");
        eval(list, n -> n > 3);

    }

    // eval 评估、评价
    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer n : list) {

            if (predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }
}

@FunctionalInterface
interface GreetingService {
    void sayMessage(String message);

    public default void upHand() {
        System.out.println("挥舞着手");
    }
}
