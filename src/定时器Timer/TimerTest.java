package 定时器Timer;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerTest {
    public static void main(String[] args) {
        /**
         * 一、Timer构造器
         * Timer用于配置用户期望的任务执行时间、执行次数、执行内容。它内部会配置,调度TimerThread、TaskQueue。
         * 1、Timer timer = new Timer(); //其中会调用this(“Timer-” + serialNumber());, 即它以Timer+序列号为该定时器的名字
         * 2、Timer timer = new Timer(String name); //以name作为该定时器的名字
         * 3、Timer timer = new Timer(boolean isDeamon); //是否将此定时器作为守护线程执行
         * 4、Timer timer = new Timer(name, isDeamon); //定时器名字, 是否为守护线程
         * 二、运行定时器
         * 1、启动一个定时器实质是启动一个线程
         * 2、所有的task都是TimerTask的子类
         * 3、所有time都是Date类型的日期
         * 4、所有delay和period都是long类型的延迟时间, 单位为毫秒
         * 三、API
         *   1、schedule
         *      1）、timer.schedule(task, time);
         *          在time时间执行task任务1次
         *      2）、timer.schedule(task, delay);
         *          在延迟delay毫秒后执行task任务1次
         *      3）、timer.schedule(task, firstTime, period);
         *          在firsttime时间执行task1次，之后定期period毫秒时间执行task, 时间如果为过去时间,不会执行过去没有执行的任务, 但是会马上执行
         *      4）、timer.schedule(task, delay, period);
         *          在延迟delay后执行task1次，之后定期period毫秒时间执行task
         */
        Timer timer = new Timer();// 在new的时候已经启动了
        
        TimerTask timerTask = new MyTimerTask("haha");
        // 当前时间开始，每隔3秒执行一次
        timer.schedule(timerTask, new Date(), 2000);// 只是添加定时任务

    }
}

class MyTimerTask extends TimerTask {

    private String name;

    public MyTimerTask(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            System.out.println("任务名称-" + name + "：startTime==" + LocalDateTime.now().getMinute() + "分"
                + LocalDateTime.now().getSecond() + "秒");
            Thread.sleep(3000);
            System.out.println("任务名称-" + name + "：startTime==" + LocalDateTime.now().getMinute() + "分"
                + LocalDateTime.now().getSecond() + "秒");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
