package 定时器Timer;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 定时任务线程池
 * @author SAMSUNG
 * 
 */
public class ScheduleThreadPoolTest {
    public static void main(String[] args) {
        ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);
        
        scheduledThreadPool.schedule(new MyTimerTask2("hehe"), 0,TimeUnit.SECONDS);
        scheduledThreadPool.scheduleAtFixedRate(new MyTimerTask2("hehe"), 0,2,TimeUnit.SECONDS);
    }
}

class MyTimerTask2 implements Runnable {

    private String name;

    public MyTimerTask2(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            System.out.println("任务名称-" + name + "：startTime==" + LocalDateTime.now().getMinute() + "分"
                + LocalDateTime.now().getSecond() + "秒");
            Thread.sleep(3000);
            System.out.println("任务名称-" + name + "：startTime==" + LocalDateTime.now().getMinute() + "分"
                + LocalDateTime.now().getSecond() + "秒");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
