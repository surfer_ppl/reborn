package 设计模式.单例模式.懒汉模式;

/**
 * 懒汉模式：被外部类调用的时候才会实例化 若果是简单的懒汉模式可能出现线程安全问题
 * 
 * @author Edward
 *
 */
public class LazySingleton {
	private static LazySingleton lazySingleton = null;
	//线程不安全
//   public static LazySingleton getInstance() {
//	   if(lazySingleton == null) {
//		   lazySingleton = new LazySingleton();
//	   }
//	   return lazySingleton;
//   }

	/*
	 * 解决方案一
	 * 使用synchronized关键字加锁
	 */
//	public synchronized static LazySimpleSingleton getInstance() {
//		if (lazySingleton == null) {
//			lazySingleton = new LazySingleton();
//		}
//		return lazySingleton;
//	}
	
	/**
	 * 解决方法二
	 * 使用双重检查锁（synchronized代码块），先判断是否已经初始化，在决定是否加锁
	 * 使用双重检查锁虽然优化了性能，但是还有一个隐患
	 * 
	 * @return
	 */
	public synchronized static LazySingleton getInstance() {
		//先判断是否存在
		if (lazySingleton == null) {
			//获取锁
			synchronized (LazySingleton.class) {
				//再次进行判断（这里的判断肯定同时只会被一个线程执行）
				if (lazySingleton == null) {
					lazySingleton = new LazySingleton();
				}
			}
		}
		return lazySingleton;
	}
	
	
	

}
