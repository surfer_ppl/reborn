package 设计模式.单例模式.懒汉模式;

public class LazySingletonTest {
	public static void main(String[] args) {
		Thread t1 = new Thread(new LazySingletonThread());
		Thread t2 = new Thread(new LazySingletonThread());
		
		t1.start();
		t2.start();
		System.out.println("end");
	}
}
