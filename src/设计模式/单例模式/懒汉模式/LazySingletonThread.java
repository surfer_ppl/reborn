package 设计模式.单例模式.懒汉模式;

public class LazySingletonThread implements Runnable{

	@Override
	public void run() {
		LazySingleton lazySimpleSingleton = LazySingleton.getInstance();
		System.out.println(Thread.currentThread().getName() +"===============>"+lazySimpleSingleton);
	}

}
