package 设计模式.单例模式.饿汉模式;
/**
 * 
 * 饿汉模式：单例对象在类加载的时候就立即初始化创建，在线程还没出现之前就实例化了，线程安全
 *          前提是没有可改变的成员变量。
 * 优点：没有加任何锁、执行效率高
 * 缺点：类加载的时候就初始化、不管用不用都占据空间、浪费内存，适用于单例模式较少的情况。
 * @author Edward
 *
 */
public class HungerSingleton1 {

	private static final HungerSingleton1 hungerSingleton1 = new HungerSingleton1();

	private HungerSingleton1() {}
	
	public static HungerSingleton1 getInstance() {
		return hungerSingleton1;
	}

}
