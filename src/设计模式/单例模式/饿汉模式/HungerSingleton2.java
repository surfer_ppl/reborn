package 设计模式.单例模式.饿汉模式;

/**
 * 第二种写法，静态代码块实例化对象
 * 
 * @author Edward
 *
 */
public class HungerSingleton2 {
	private static final HungerSingleton2 hungerSingleton;
	//使用static代码块初始化
	static {
		hungerSingleton = new HungerSingleton2();
	}

	private HungerSingleton2() {
	}

	public static HungerSingleton2 getInstance() {
		return hungerSingleton;
	}
}
