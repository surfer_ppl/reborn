package 设计模式.建造者模式;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 使用Lombook
 * @author Edward
 * @date 2022/09/15
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class People3 {
    /**
     * 姓名
     */

    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 籍贯
     */
    private String address;

    /**
     * 身份证
     */
    private String idNum;
    
}
