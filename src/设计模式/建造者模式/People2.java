package 设计模式.建造者模式;

/**
 * 建造者模式People
 * @author SAMSUNG
 *
 */
public class People2 {
    /**
     * 姓名
     */

    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 籍贯
     */
    private String address;

    /**
     * 身份证
     */
    private String idNum;
    
    
    public People2() {
        super();
    }

    private People2(Builder builder) {
        this.name = builder.name;
        this.age = builder.age;
        this.gender = builder.gender;
        this.address = builder.address;
    }
    
    //因为set属性已经在builder中设置，所以这里只进行get
    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getIdNum() {
        return idNum;
    }

    @SuppressWarnings("unused")
    public static class Builder {
        /**
         * 姓名
         */

        private String name;
        /**
         * 年龄
         */
        private Integer age;
        /**
         * 性别
         */
        private String gender;
        /**
         * 籍贯
         */
        private String address;

        /**
         * 身份证
         */
        private String idNum;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAge(Integer age) {
            this.age = age;
            return this;
        }

        public Builder setGender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setIdNum(String idNum) {
            this.idNum = idNum;
            return this;
        }

        public People2 bulid() {
            // 在返回对象前可以对属性进行一些处理，一般为校验
            return new People2(this);
        }

    }
}
