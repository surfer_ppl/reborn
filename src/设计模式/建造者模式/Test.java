package 设计模式.建造者模式;

import 设计模式.建造者模式.People2.Builder;

public class Test {
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        //构造参数初始化对象
        People people = new People("张之维", 100, "男");
        People people1 = new People("张楚岚", 20, "男","哪儿都通物流","131001228");
        
        //set初始化对象
        People people3 = new People();
        people3.setName("冯宝宝");
        people3.setAge(18);
        people3.setGender("女");
        people3.setAddress("哪儿都通物流");
        
        /**
         * 
         * 不同people的这几个的值是不一样的。如果不使用builder模式，那么很可能在创建很多对象的时候代码量很多且冗余，
         * 而且假如想要创建一个address都是哪都通物流的人们，
         * 那么就要在每创建一次对象的时候赋值一次，十分不灵活。
         * 但是如果使用builder模式，这些问题就都可以得到解决。
         * 
         */
        Builder builder = new People2.Builder().setName("张灵玉").setGender("男").setAge(23).setAddress("龙虎山");
        People2 buildPeople = builder.bulid();
        System.out.println(buildPeople.getAddress());
        
        
        People3 buildPeople1 = People3.builder().name("夏禾").gender("女").age(21).address("全性").build();
        System.out.println(buildPeople1.getAddress());
    }
}
