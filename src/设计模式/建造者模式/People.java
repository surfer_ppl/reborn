package 设计模式.建造者模式;
/**
 *  构建对象，给属性赋值，可以
    1、使用包含形参的构造器，弊端是需要构造多个不同形参的方法;
    2、一个空的构造器，然后使用setter方法进行设置，弊端是使用时冗长。
    因此可以使用builder来简化代码。
 * @author SAMSUNG
 */
public class People {
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 籍贯
     */
    private String address;
    
    /**
     * 身份证
     */
    private String idNum;
    
    
    /**
     * 全部参数的构造器
     * @param name
     * @param age
     * @param gender
     * @param address
     * @param idNum
     */
    public People(String name, Integer age, String gender, String address, String idNum) {
        super();
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.address = address;
        this.idNum = idNum;
    }
    /**
     * 只有性别年龄的构造器
     * @param name
     * @param age
     * @param gender
     */
    public People(String name, Integer age, String gender) {
        super();
        this.name = name;
        this.age = age;
        this.gender = gender;
    }
    
    public People() {
    }
    
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getIdNum() {
        return idNum;
    }
    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }  
    
}
