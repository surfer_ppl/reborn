package 算法.排序算法.快速排序;

/**
 * 
 * @author Edward
 *
 */
public class QuickSort {
	public static void main(String[] args) {

		/**
		 * 其基本思想是：选取第一个数作为基数，通过一趟排序将要排序的数据分割成独立的两部分， 其中一部分的所有数据都比另外一部分的所有数据都要小，
		 * 一般左边存放比基数小或者等于的数，右边放比基数大的数
		 * 然后再按此方法对这两部分数据分别进行快速排序，整个排序过程可以递归进行，以此达到整个数据变成有序序列。
		 * 
		 * 从快速排序的基本思想可以分析出其实现思路：
		 * 
		 * 一、选取一个也叫基准元素拿出来（基准元素的位置就成了一个坑位），定位最左和最右两个指针，（目前最左的指针指向的是一个坑位）
		 * 
		 * 二、先从右边开始遍历，如果大于基数，指针左移，如果小于基数，把当前的数放在之前的坑位里，当前数所在位置变成新的坑位，左指针右移动，然后结束右遍历
		 * 从左开始遍历，如果单前数比基数小，指针右移动，如果大于基数，把当前的数放在之前的坑位里，当前数所在位置变成新的坑位，右指针左移，然后结束左遍历，继续右遍历
		 * 
		 * 三、将数组分割成两部分，一部分数据都小于或等于基础元素，另一部分数据都大于基础元素
		 * 
		 * 四、对分割的子数组递归地执行步骤一二，直到无法分割
		 */

		int nums[] = { 49, 38, 65, 97, 76, 13, 27, 49 };
		quickSort(nums);

		System.out.println("排序后如下");
		for (int num : nums) {
			System.out.print(num + "\t");
		}

	}

	/**
	 * 既然用到递归，那就要封装一个方法
	 * 
	 * @param intArr 要排序的数组
	 * @param low    低位(0)
	 * @param hight  高位(数组长度)
	 */
	public static void sort(int[] nums, int startIndex, int endIndex) {

		// 定义两个变量重新接收开始和结束下标
		int left = startIndex;
		int right = endIndex;

		// 定义递归结束条件
		if (left >= right) {
			return;
		}

		// 以第一个数作为基数
		int baseNum = nums[left];
		// 以第一个元素的下标(最左边)作为第一个Index
		int index = left;
		// 从表的两端向中间进行left增right减(left++ right--)
		while (right > left) {
			while (right > left) {
				// right指针从右向左进行比较,小于基数，坑位指针指向单前，单前数填充前坑位，大于基数，指针左移
				if (nums[right] < baseNum) {
					nums[index] = nums[right];
					index = right;
					left++;
					break;
				} else {
					right--;
				}
			}
			while (left < right) {
				// left指针从左向右进行比较,大于基数，坑位指针指向当前，单前数填充前坑位，小于基数，指针右移
				if (nums[left] > baseNum) {
					nums[index] = nums[left];
					index = left;
					right--;
					break;
				} else {
					left++;
				}
			}
		}
		// 最终坑位指针index会指向中间（有多个相同的数，指向最后一个）,把取出来基数替换回去
		nums[index] = baseNum;

		// 然后开始递归调用，对左边的进行如上相同操作
		sort(nums, startIndex, index - 1);
		// 对右边的进行如上相同操作
		sort(nums, index + 1, endIndex);
	}

	/**
	 * 继续封装一层，直接传入需要排序的数组即可
	 * 
	 * @param nums
	 */
	public static void quickSort(int[] nums) {
		sort(nums, 0, nums.length - 1);
	}

}
