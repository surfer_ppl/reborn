package 算法.排序算法.冒泡排序;

/**
 * 
 * @author Edward
 */
public class BubbleSort {
    public static void main(String[] args) { 
        /**
         * 排序思路：
         * 比较相邻的元素。如果第一个比第二个大，就交换他们两个。
         * 对每一对相邻元素作同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。
         * 
         * 第一次循环用10和后面的数进行比较，比后面的大就交换位置 第一次结果：10,8,16,7,21,12,23 
         * 第二次循环用8和后面的树进行比较，比后面的大就交换位置 第二次结果：8,10,7,16,12,21,23
         * 第三次循环……
         * 10,8,16,7,21,12,23 
         * 8,10,7,16,12,21,23 
         * 8,7,10,12,16,21,23 
         * 7,8,10,12,16,21,23 
         * 7,8,10,12,16,21,23
         * 7,8,10,12,16,21,23 
         * 7,8,10,12,16,21,23

         */
    	int[] nums = new int[] {10, 21, 8, 16, 7, 23, 12};
        //int[] nums = new int[] {8,7,10,12,16,21,23};
        
    	
    	for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length - 1 - i; j++) {
                if (nums[j] > nums[j + 1]) {
                    int tmpNum = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = tmpNum;
                }
            }
            //每一次排序后打印结果
            System.out.println();
            for (int num : nums) {
                System.out.print(num+",");
            }
            
        }
        
        System.out.println("排序后如下");
        for (int num : nums) {
            System.out.print(num+"\t");
        }
    }
}
