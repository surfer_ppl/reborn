package 数据结构.二叉树;


import 数据结构.二叉树.链式结构.ChainBinaryTree;
import 数据结构.二叉树.链式结构.Node;

public class TreeTest {
    public static void main(String[] args) {
        ChainBinaryTree chainBinaryTree = new ChainBinaryTree();
        
        chainBinaryTree.insert(4);
        chainBinaryTree.insert(2);
        chainBinaryTree.insert(3);
        chainBinaryTree.insert(3);
        chainBinaryTree.insert(4);
     
        
        
        Node root = chainBinaryTree.find(2);
        System.out.println(root.leftNode.getData());
        System.out.println(root.rightNode.getData());
        
        
    }
}
