package 数据结构.二叉树.链式结构;

/**
 * 定义二叉树的节点(以int类型为例)
 * @author Edward
 *
 */
public class Node {
	//节点数据
	private int data;
	//左节点
	public Node leftNode;
	//右节点
	public Node rightNode;
	
	public Node(int data) {
		super();
		this.data = data;
	}

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getLeftNode() {
		return leftNode;
	}

	public void setLeftNode(Node leftNode) {
		this.leftNode = leftNode;
	}

	public Node getRightNode() {
		return rightNode;
	}

	public void setRightNode(Node rightNode) {
		this.rightNode = rightNode;
	}
	
	
	
	
	
	
	
	
    
	
	
}
