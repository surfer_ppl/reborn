package 数据结构.二叉树.链式结构;

import 数据结构.二叉树.BinaryTree;

/**
 * 链式结构二叉树
 * 
 * @author Edward
 *
 */
public class ChainBinaryTree extends BinaryTree {
	/**
	 * 根节点
	 */
	private Node root;

	/**
	 * 查找节点从根节点开始遍历查找 
	 * 1、查找值比当前节点值大，则搜索右子树； 
	 * 2、查找值等于当前节点值，停止搜索（终止条件）；
	 * 3、查找值小于当前节点值，则搜索左子树；
	 */
	@Override
	public Node find(int data) {
		// 定义当前节点为根节点
		Node current = this.root;
		while (current != null) {
			if (current.getData() > data) {
				// 要查找的数小于当前节点，则继续遍历左子树
				current = current.getLeftNode();
			} else if (current.getData() < data) {
				// 要查找的数大于当前节点，则继续遍历右子树
				current = current.getRightNode();
			} else {
				return current;
			}
		}
		// 查不到返回null
		return null;
	}

	/**
	 * 要插入节点，必须先找到插入的位置,操作与查找类似，等待插入的节点也是从根节点开始比较， 
	 * 小于的放左边大于的放右边，直到左子树为空或者右子树为空，然后存放，
	 * 过程中要随时保存父节点的信息。
	 * 
	 * 此实现为二叉搜索树，无重复数据
	 */
	@Override
	public boolean insert(int data) {
		// 要先判断根节点是否为空，设置第一个节点为根节点
		Node newNode = new Node(data);
		if (this.root == null) {
			this.root = newNode;
			return true;
		}
		// 根节点不为空，设置单前节点为根节点，开始查找插入位置
		Node current = root;
		// 定义父节点,时刻保存节点信息
		Node parentNode = null;

		while (current != null) {
			parentNode = current;
			if (current.getData() > data) {
				// 小于当前节点，看左子树是否存在
				current = current.getLeftNode();
				// 不存在，直接放
				if (current == null) {
					parentNode.leftNode = newNode;
					return true;
				}
			} else {
				// 大于当前节点，看右子树是否存在
				current = current.getRightNode();
				// 不存在，直接放
				if (current == null) {
					parentNode.rightNode = newNode;
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 首先要找到需要删除的数据 
	 * 1、该节点是叶子节点 
	 * 2、该节点有一个叶子节点 
	 * 3、该节有两个叶子节点
	 */
	@Override
	public boolean delete(int data) {
		// 从根节点开始
		Node current = this.root;
		Node parentNode = this.root;
		// 如果当前节点（根节点）为空直接返回fasle
		if (current == null) {
			return false;
		}
		// 定义一个值判断单前节点是左还是右
		boolean isLeft = false;
		// 定位data的位置
		while (current.getData() != data) {
			
			// 小就找左树
			if (current.getData() > data) {
				isLeft = true;
				// 左节点为当前节点
				current = current.leftNode;
			} else {
				// 大就找右树
				// 右节点为单前节点
				isLeft = false;
				current = current.rightNode;
			}
			//父节点为当前节点
			parentNode = current;
			// 判断单前节点是否为空,证明没有节点了
			if (current == null) {
				return false;
			}

		}
		//如果等于后开始证明找到了对应的节点，开始判断是什么类型的节点
		//此节点为叶子节点
		if(current.leftNode == null && current.rightNode == null) {
			//如果是根节点直接删除根节点
			if(current == this.root) {
				this.root = null;
			}else if(isLeft) {
				parentNode.leftNode = null;
			}else {
				parentNode.rightNode = null;
			}
			return true;
		}
		
		//单前节点有一个节点（可能只有一个左节点，可能只有一个右节点）
		if(current.leftNode != null && current.rightNode == null) {
			//如果删除的是根节点，那么根节点的子节点将成为新的根节点
			if(current == this.root) {
				this.root = current.leftNode;
			}else if(isLeft) {
				//将当前节点的左节点变为单前节点父节点的左子节点替换当前节点
				parentNode.leftNode = current.leftNode;
			}else {
				//将当前节点的右节点变为单前节点父节点的右子节点替换当前节点
				parentNode.rightNode = current.leftNode;
			}
		}else if(current.leftNode == null && current.rightNode != null) {
			//如果删除的是根节点，那么根节点的子节点将成为新的根节点
			if(current == this.root) {
				this.root = current.rightNode;
			}else if(isLeft) {
				//将当前节点的右节点变为单前节点父节点的左子节点替换当前节点
				parentNode.leftNode = current.rightNode;
			}else {
				//将当前节点的右节点变为单前节点父节点的右子节点替换当前节点
				parentNode.rightNode = current.rightNode;
			}
		}
		
		//单前节点有两个节点
		if(current.leftNode != null && current.rightNode != null) {
			//获取删除节点的后续节点
		}
		
		
		return false;
	}
	
	/**
	 * 获取后续的节点
	 * @param delNode
	 * @return
	 */
	public Node getFollower(Node delNode){
		return delNode;
		
	}

}
