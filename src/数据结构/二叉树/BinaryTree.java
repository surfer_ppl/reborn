package 数据结构.二叉树;

import 数据结构.二叉树.链式结构.Node;

/**
 * 
 * @author Edward
 *
 */
public abstract class BinaryTree {
	
	public int count = 0;
	
	/**
	 * 想要存储一颗二叉树，一般有两种方法。 
	 * 一、基于引用的链式存储方式 每个节点至少三个字段，其中一个存储节点数据，
	 * 另外两个是指向左右子节点的引用，我们只需要根据节点，就可以通过左右子节点的指针 把整棵树都串起来。这种方式比较符合逻辑也比较常用
	 * 
	 * 二、使用数组的方式进行存储 把根节点A存储在下标是i=1的位置，该节点的左子节点B存储在下标2*i=2的位置，右子节点C存储在2*i+1 = 3的位置，
	 * 以此类推B节点的左子节点存储在2*i=4的位置，右子节点存储在2*i+1=5的位置 …… 根据单前节点的下标除以2就可以得到上一个节点的位置
	 */
	
	/**
	 * 查找二叉树节点
	 * @param data
	 * @return Node
	 */
	public abstract Node find(int data);
	/**
	 * 插入
	 * @param data
	 * @return
	 */
	public abstract boolean insert(int data);
	/**
	 * 删除
	 * @param data
	 * @return
	 */
	public abstract boolean delete(int data);
	
	/**
	 * 节点总个数
	 * @return
	 */
	public int count() {
		return this.count;
	}
	

}
