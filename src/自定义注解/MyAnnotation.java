package 自定义注解;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 自定义注释一些浅浅的解析
 * @author Edward
 * @date 2022/12/14
 */
@Target({ElementType.FIELD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MyAnnotation {

    /**
     * 1、注释的作用：
     *     像平时用到的 springboot、mybatis 等框架提供了许多的注解，
     * 免去了许多配置文件的繁琐工作，大大简便了开发，Java 提供了自定义注解的功能
     * 注解可以看作是一种特殊的标记，可以用在方法、类、参数和包上，
     * 程序在编译或者运行时可以检测到这些标记而进行一些特殊的处理，
     * 例如标注在方法上可以实现接口权限的校验，在参数上实现格式或非空校验等
     * 
     * 2、声明方式：通过关键字 @interface 声明为注解
     * 
     * 3、注解的元素类型
     *     主要有@Target，@Retention,@Document,@Inherited 用来修饰注解。
     * 3.1 @Target 表明该注解可以应用的java元素类型
     *     ElementType.TYPE                 应用于类、接口（包括注解类型）、枚举
     *     ElementType.FIELD                应用于属性（包括枚举中的常量）
     *     ElementType.METHOD               应用于方法
     *     ElementType.PARAMETER            应用于方法的形参
     *     ElementType.CONSTRUCTOR          应用于构造函数
     *     ElementType.LOCAL_VARIABLE       应用于局部变量
     *     ElementType.ANNOTATION_TYPE      应用于注解类型
     *     ElementType.PACKAGE              应用于包
     *     ElementType.TYPE_PARAMETER       1.8版本新增，应用于类型变量
     *     ElementType.TYPE_USE             1.8版本新增，应用于任何使用类型的语句中（例如声明语句、泛型和强制转换语句中的类型）
     *     
     * 3.2 @Retention 表明该注解的生命周期   
     *     RetentionPolicy.SOURCE            编译时被丢弃，不包含在类文件中
     *     RetentionPolicy.CLASS             JVM加载时被丢弃，包含在类文件中，默认值
     *     RetentionPolicy.RUNTIME           由JVM 加载，包含在类文件中，在运行时可以被获取到
     *     
     * 3.3 @Documented 表明该注解标记的元素可以被Javadoc 或类似的工具文档化。
     *     
     * 3.4 @Inherited /ɪnˈherɪtɪd/  遗传  表明使用了@Inherited注解的注解，所标记的类的子类也会拥有这个注解
     *         
     */

    String value() default "初始化ing……";
    boolean isDelete();

}
