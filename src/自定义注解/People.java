package 自定义注解;

import lombok.Builder;
import lombok.Data;

/**
 * 使用Lombook
 * @author Edward
 * @date 2022/09/15
 */
@Builder
@Data
@MyAnnotation(isDelete = false)
public class People {
    /**
     * 姓名
     */

    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private String gender;
    /**
     * 籍贯
     */
    private String address;

    /**
     * 身份证
     */
    private String idNum;
    
    /**
     * 是否注销
     */
    private Boolean isDelete;
    
}
