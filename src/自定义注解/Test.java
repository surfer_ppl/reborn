package 自定义注解;

/**
 * 自定义注解测试
 * @author Edward
 * @date 2022/12/14
 */
public class Test {
    public static void main(String[] args) {
        try {
            // 获取Person的Class对象
            People person = People.builder().build();
            Class<? extends People> clazz = person.getClass();
            // 判断person对象上是否有Info注解
            if (clazz.isAnnotationPresent(MyAnnotation.class)) {
                System.out.println("Person类上配置了Info注解！");
                // 获取该对象上Info类型的注解
                MyAnnotation infoAnno = (MyAnnotation)clazz.getAnnotation(MyAnnotation.class);
                person.setName(infoAnno.value());
                person.setIsDelete(infoAnno.isDelete());
                System.out.println(person);
                System.out.println("person.name :" + infoAnno.value() + ",person.isDelete:" + infoAnno.isDelete());
            } else {
                System.out.println("Person类上没有配置Info注解！");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
