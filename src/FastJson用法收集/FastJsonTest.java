package FastJson用法收集;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

public class FastJsonTest {
    @SuppressWarnings("unused")
    public static void main(String[] args) {
        List<Student> students = StudentList.getStudents();
        Student stuZcl = Student.builder().name("张楚岚").age(18).genger("男").failCount(0).supplyTeacher("张怀义")
            .headTeacher("张之为").build();
        /**
         * 1、传入一个对象，将对象转换成Json字符串
         */
        String studentsStr = JSON.toJSONString(students);
        String stuStr = JSON.toJSONString(stuZcl);
        System.out.println(stuStr);
        System.out.println(studentsStr);
        
        /**
         * 2、JSONObject、JSONArray 是json的两个子类
         *    JSONObject相当于 Map<String, Object>
         *    JSONArray相当于 List<Object>
         */
        JSONObject parseObject = JSONObject.parseObject(stuStr);  
        JSONArray studentsJsonArray = JSONArray.parseArray(studentsStr);
        /**
         * 3、指定类型直接转换成对应的类型
         */
        Student student = JSONObject.parseObject(stuStr,Student.class);
        List<Student> student2 = JSONArray.parseArray(studentsStr,Student.class);
        System.out.println(student);
        System.out.println(student2);
        
        
        /**
         * SerializerFeature
         * fastjson通过SerializerFeature对生成的json格式的数据进行一些定制，比如可以输入的格式更好看，使用单引号而非双引号等。
         * 
         * QuoteFieldNames                  输出key时是否使用双引号,默认为true
         * UseSingleQuotes                  使用单引号而不是双引号,默认为false
         * WriteMapNullValue                是否输出值为null的字段,默认为false
         * WriteEnumUsingToString           Enum输出name()或者original,默认为false
         * UseISO8601DateFormat             Date使用ISO8601格式输出，默认为false
         * WriteNullListAsEmpty             List字段如果为null,输出为[],而非null
         * WriteNullStringAsEmpty           字符类型字段如果为null,输出为”“,而非null
         * WriteNullNumberAsZero            数值字段如果为null,输出为0,而非null
         * WriteNullBooleanAsFalse          Boolean字段如果为null,输出为false,而非null
         * SkipTransientField               如果是true，类中的Get方法对应的Field是transient，序列化时将会被忽略。默认为true
         * SortField                        按字段名称排序后输出。默认为false
         * WriteTabAsSpecial                把\t做转义输出，默认为false不推荐设为true
         * PrettyFormat                     结果是否格式化,默认为false
         * WriteClassName                   序列化时写入类型信息，默认为false。反序列化是需用到
         * DisableCircularReferenceDetect   消除对同一对象循环引用的问题，默认为false
         * WriteSlashAsSpecial              对斜杠’/’进行转义
         * BrowserCompatible                将中文都会序列化为 \\uXXXX 格式，字节数会多一些，但是能兼容IE 6，默认为false
         * WriteDateUseDateFormat           全局修改日期格式,默认为false。
         * DisableCheckSpecialChar          一个对象的字符串属性中如果有特殊字符如双引号，将会在转成json时带有反斜杠转移符。如果不需要转义，可以使用这个属性。默认为false
         * BeanToArray                      将对象转为array输出
         */
        
        String studentsStr1 =  JSON.toJSONString(student,SerializerFeature.WriteClassName);
        System.out.println(studentsStr1);
        
        String studentsStr2 = JSON.toJSONString(students,SerializerFeature.UseSingleQuotes);
        System.out.println(studentsStr2);
        
        Student student3 = JSON.parseObject(studentsStr1,Student.class);
        System.out.println(student3);
        
        
        
        
        
        
    }
}
