package FastJson用法收集;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Builder
@Getter
@ToString
public class Student {
    /**
     * 名字
     */
    private String name;
    /**
     * 性别
     */
    private String genger;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 不及格次数
     */
    private Integer failCount;
    /**
     * 代课老师
     */
    private String supplyTeacher;
    /**
     * 班主任
     */
    private String headTeacher;
    
}

class StudentList {
    public static List<Student> getStudents() {
        Student stuBo = Student.builder().name("布欧").age(18).genger("男").failCount(10).supplyTeacher("小界王")
            .headTeacher("比鲁斯").build();
        Student stuKklt = Student.builder().name("卡卡罗特").age(18).genger("男").failCount(7).supplyTeacher("小界王")
            .headTeacher("比鲁斯").build();
        Student stuBjt = Student.builder().name("贝吉塔").age(18).genger("男").failCount(5).supplyTeacher("小界王")
            .headTeacher("比鲁斯").build();
        Student stuKulin = Student.builder().name("克林").age(18).genger("男").failCount(1).supplyTeacher("大界王")
            .headTeacher("维斯").build();
        Student stuGxr = Student.builder().name("龟仙人").age(18).genger("男").failCount(2).supplyTeacher("大界王")
            .headTeacher("维斯").build();
        Student stuBm = Student.builder().name("布玛").age(18).genger("女").failCount(0).supplyTeacher("大界王")
            .headTeacher("维斯").build();
        Student stuKn = Student.builder().name("柯南").age(18).genger("男").failCount(0).supplyTeacher("毛利小五郎")
            .headTeacher("目暮警官").build();
        Student stuHya = Student.builder().name("灰原哀").age(18).genger("女").failCount(0).supplyTeacher("毛利小五郎")
            .headTeacher("目暮警官").build();

        List<Student> students = new ArrayList<>();
        students.add(stuBo);
        students.add(stuKklt);
        students.add(stuBjt);
        students.add(stuKulin);
        students.add(stuGxr);
        students.add(stuBm);
        students.add(stuKn);
        students.add(stuHya);

        return students;

    }

}
