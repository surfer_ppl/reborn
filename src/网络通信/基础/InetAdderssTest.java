package 网络通信.基础;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 通行要素一：IP地址
 * InetAddress 封装了IP 并且为了区分IPV4和IPV6,
 * java提供了两个InetAddress的子类：Inet4Address、Inet6Address 
 * 比如：File类封装了硬盘上的一个文件new File("fielPath")
 * 
 * @author Edward
 *
 */
public class InetAdderssTest {
	public static void main(String[] args) {
		/*
		 * InetAddress inetAddress = newInetAddress();
		 * 不能直接创建对象，因为InetAddress()被deafult修饰了，只能同一个包内使用
		 */
		try {
			InetAddress inet1 = InetAddress.getByName("192.168.31.22");
			System.out.println(inet1.getHostName());
			//www.baidu.com 需要进行DNS解析所以必须连网，否则会报错
			InetAddress inet2 = InetAddress.getByName("www.baidu.com");
			System.out.println(inet2); 
			InetAddress inet3 = InetAddress.getByName("127.0.0.1");
			System.out.println(inet3);
			//如果安装了虚拟机则会受到虚拟机的干扰，禁用虚拟网卡即可
			//如果连接了网络，返回的则是对外提供的真实的IP
			//断网状态下返回的是127.0.0.1
			InetAddress inet4 = InetAddress.getLocalHost();
			System.out.println(inet4);
			/**
			 * 获取到IP对象后，常用的两个方法
			 * getHostName() 获取解析后的主机名
			 * getHostAddress() 获取解析后的IP地址
			 */
			String hostName = inet2.getHostName();
			String hostAddress = inet2.getHostAddress();
			System.out.println(hostName);
			System.out.println(hostAddress);
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	

}
